﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace np_lesson4
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket srvSocket = new Socket(AddressFamily.InterNetwork,SocketType.Dgram,ProtocolType.Udp);
            srvSocket.Bind(new IPEndPoint (IPAddress.Any, 12345));
            EndPoint clientEndPoint = new IPEndPoint(0, 0);
            byte[] buf = new byte[64 * 1024];
            int recSize= srvSocket.ReceiveFrom(buf, ref clientEndPoint);
            Console.WriteLine(Encoding.UTF8.GetString(buf, 0, recSize));
            Console.ReadLine();
        }
    }
}
